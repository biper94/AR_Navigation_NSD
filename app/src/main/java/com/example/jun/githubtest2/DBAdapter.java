package com.example.jun.githubtest2;


import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class DBAdapter extends CursorAdapter {



    public DBAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.search_item, parent, false);

        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final TextView name = (TextView)view.findViewById(R.id.search_item_text);
        name.setText(cursor.getString(cursor.getColumnIndex("name")) );

    }
    @Override
    public String getItem(int pos) {
        Cursor cursor = getCursor();
        if (cursor.moveToPosition(pos)) {
            return cursor.getString(cursor.getColumnIndex("name"));
        }
        else {
            Log.e("DBAdapter", "cursor moving error!!");
            return null;
        }
    }
}
