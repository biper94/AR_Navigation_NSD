package com.example.jun.githubtest2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jun on 2017-06-25.
 */

public class RouteList implements Serializable{
    public LinkedList<Point> list;

    public RouteList(LinkedList<Point> list) {
        this.list = list;
    }

    public List<Point> getList() {
        return list;
    }
}
