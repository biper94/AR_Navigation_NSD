package com.example.jun.githubtest2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.kaopiz.kprogresshud.KProgressHUD;

public class MainActivity extends AppCompatActivity implements LocationListener, View.OnClickListener{
    private Button startBtn;
    private TextView sourceLocation;
    private TextView destLocation;
    private Point curLoc;
    static final int REQUEST_SOURCE_CODE = 44;
    static final int REQUEST_DEST_CODE = 55;
    KProgressHUD hud;
    LocationManager locMgr;
    HttpURLConnection conn;
    LinkedList<Point> list = new LinkedList<>();
    double lat;
    double lng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String sourceDefaultText = "위치요청 중..";
        //requestAllPermissions();

        startBtn = (Button)findViewById(R.id.startButton);
        startBtn.setOnClickListener(this);

        sourceLocation = (TextView)findViewById(R.id.sourceText);
        sourceLocation.setText(sourceDefaultText);

        destLocation = (TextView)findViewById(R.id.destText);
        destLocation.setOnClickListener(this);

        locMgr = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Routing..\nPlease wait..");
        requestLocation();
    }
    @Override
    public void onClick(View v) {
        if (v == destLocation) {
            Intent intent = new Intent(MainActivity.this, SearchDestActivity.class);
            startActivityForResult(intent, REQUEST_DEST_CODE);
        }
        else if (v == startBtn) {

            hud.show();
            Thread thread = new Thread(new ApiThread(curLoc, new Point(lat, lng, null), conn, list, getApplicationContext()));
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (list.size() == 0) {
                Intent noRoute = new Intent(MainActivity.this, FailActivity.class);
                startActivity(noRoute);
                finish();
            }
            RouteList route = new RouteList(list);
            Intent intent = new Intent(MainActivity.this, Compass.class);
            intent.putExtra("Route", route);
            Log.e("JSON", String.valueOf(list.size()));
            for (int i = 0; i < list.size(); i++) {
                Log.e("Mid Spot", ""+ list.get(i).latitude + ", " + list.get(i).longitude
                    + list.get(i).description);
            }
            hud.dismiss();
            startActivity(intent);

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_SOURCE_CODE && resultCode == RESULT_OK){
            sourceLocation.setText(intent.getStringExtra("Source"));
        }
        else if (requestCode == REQUEST_DEST_CODE && resultCode == RESULT_OK) {
            destLocation.setText(intent.getStringExtra("DestOut"));
            lat = Double.parseDouble(intent.getStringExtra("lat"));
            lng = Double.parseDouble(intent.getStringExtra("lng"));
            //Toast.makeText(getApplicationContext(), "Dest: " + lat + ", " + lng, Toast.LENGTH_SHORT).show();
        }
    }
    public void requestLocation() {
        try {
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, this);
            locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        curLoc = new Point(location.getLatitude(), location.getLongitude(), "현재 위치");
        sourceLocation.setText("" + curLoc.latitude + " " + curLoc.longitude);
        try {
            locMgr.removeUpdates(this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, this);
            locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            locMgr.removeUpdates(this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            locMgr.removeUpdates(this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }
    }


}
