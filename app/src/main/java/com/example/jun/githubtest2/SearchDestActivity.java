package com.example.jun.githubtest2;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SearchDestActivity extends AppCompatActivity {
    EditText edit;
    ImageButton searchBtn;
    ImageButton backBtn;
    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private DBAdapter adapter;
    ListView listView;
    TextView recentTextView;

    int selec = 0;
    String lats[];
    String lngs[];
    String addresses[];
    public static String defaultUrl = "https://apis.daum.net/local/v1/search/keyword.json?apikey=633886287cfa03f9c8ed9a5e7fe978ce&query=";
    Handler handler = new Handler();
    int jsonResultsLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_dest);
        recentTextView = (TextView)findViewById(R.id.latest_text);
        recentTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowList();
            }
        });
        dbHelper = new DBHelper(SearchDestActivity.this, "dest.db", null, 1);

        db = dbHelper.getWritableDatabase();
        dbHelper.onCreate(db);
        edit = (EditText)findViewById(R.id.dest_edit);

        searchBtn = (ImageButton)findViewById(R.id.search_dest_btn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();

                    values.put("name", edit.getText().toString());
                    db.insert("dest", null, values);
                    String userSTr = edit.getText().toString();
                    String urlStr = defaultUrl + userSTr;
                    ConnectThread thread = new ConnectThread(urlStr);
                    thread.start();


                }
        });
        listView = (ListView)findViewById(R.id.searchedList);
        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = db.query("dest", null, null, null, null, null, null, null);
                DBAdapter adapter = new DBAdapter(SearchDestActivity.this, c);

                String str = adapter.getItem(position);
                if (str == null)
                    Toast.makeText(getApplicationContext(), "Error on getItem in single click", Toast.LENGTH_LONG).show();
                else {
                    /*
                    Intent outIntent = getIntent();
                    outIntent.putExtra("DestOut", str);
                    setResult(RESULT_OK, outIntent);
                    finish();*/
                    Toast.makeText(getApplicationContext(), "Dest: " +  str, Toast.LENGTH_SHORT).show();
                    String urlStr = defaultUrl + str;
                    ConnectThread thread = new ConnectThread(urlStr);
                    thread.start();
                }
            }
        });

        listView.setOnItemLongClickListener(new ListView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = db.query("dest", null, null, null, null, null, null, null);
                DBAdapter adapter = new DBAdapter(SearchDestActivity.this, c);
                String str = adapter.getItem(position);
                if (str == null) {
                    Toast.makeText(getApplicationContext(), "Error on getItem in long click", Toast.LENGTH_LONG).show();
                    return false;
                }
                else {
                    db.delete("dest", "name=?", new String[] { str });
                    Toast.makeText(getApplicationContext(), str + " Successfully deleted!", Toast.LENGTH_SHORT).show();
                    ShowList();
                    return true;
                }

            }
        });
        backBtn = (ImageButton)findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent outIntent = getIntent();
                setResult(RESULT_CANCELED, outIntent);
                finish();
            }
        });
        ShowList();
    }
    private void ShowList() {
        Cursor c = db.query("dest", null, null, null, null, null, null, null);
        c.moveToPosition(0);
        DBAdapter adapter = new DBAdapter(SearchDestActivity.this, c);
        ListView list = (ListView)findViewById(R.id.searchedList);
        list.setAdapter(adapter);
        try {
            Log.e("Current Cursor Position", String.valueOf(c.getInt(0)));
        } catch(android.database.CursorIndexOutOfBoundsException e) {
            Log.e("Current Cursor Position", "-1, index out of bounds exception");
        }

    }
    class ConnectThread extends Thread{
        String urlStr;
        public ConnectThread(String inStr){
            urlStr = inStr;
        }
        public void run(){
            try{
                final String output = request(urlStr);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        findLatLng(output);
                    }
                });
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }

        private String request(String urlStr){
            StringBuilder output = new StringBuilder();
            try{
                URL url = new URL(urlStr);
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                if(conn != null){
                    conn.setConnectTimeout(10000);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setRequestProperty("Accept-Charset", "UTF-8");

                    int resCode = conn.getResponseCode();

                    Log.d("resCode", String.valueOf(resCode));
                    if(resCode == HttpURLConnection.HTTP_OK){
                        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String line = null;
                        while(true){
                            line = reader.readLine();
                            if(line == null){
                                break;
                            }
                            output.append(line + "\n");
                        }

                        reader.close();
                        conn.disconnect();
                    }
                }
            } catch (Exception ex){
                Log.e("SampleHTTP", "Exception in processing response.", ex);
                ex.printStackTrace();
            }

            return output.toString();
        }
    }
    private void findLatLng(String output){
        Log.d("output", output);
        try{
            JSONObject jsonObject = new JSONObject(output);
            JSONObject channel = new JSONObject(jsonObject.getString("channel"));
            JSONArray items = new JSONArray(channel.getString("item"));
            jsonResultsLength = items.length();
            if(jsonResultsLength > 0){
                addresses = new String[jsonResultsLength];
                lats = new String[jsonResultsLength];
                lngs = new String[jsonResultsLength];
                for(int i = 0; i < jsonResultsLength ; i++){
                    String address = items.getJSONObject(i).getString("title");
                    String lat = items.getJSONObject(i).getString("latitude");
                    String lng = items.getJSONObject(i).getString("longitude");
                    addresses[i] = address;
                    lats[i] = lat;
                    lngs[i] = lng;
                    Log.d("lats", lat);
                    Log.d("lngs", lng);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        Intent intent = new Intent(this, SearchDestActivity2.class);
        intent.putExtra("size", jsonResultsLength);
        intent.putExtra("addresses", addresses);
        intent.putExtra("lats", lats);
        intent.putExtra("lngs", lngs);
        intent.putExtra("search", edit.getText().toString());
        startActivityForResult(intent, 10);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 10 && resultCode == RESULT_OK){
            String lat = intent.getStringExtra("DestLat");
            String lng = intent.getStringExtra("DestLng");
            String dest = intent.getStringExtra("Dest");
            Intent tmp = getIntent();
            tmp.putExtra("lat",lat);
            tmp.putExtra("lng", lng);
            tmp.putExtra("DestOut", dest);
            setResult(RESULT_OK,tmp);
            finish();
        }
    }


}