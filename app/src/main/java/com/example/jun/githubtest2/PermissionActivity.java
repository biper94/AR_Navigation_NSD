package com.example.jun.githubtest2;


import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import com.kaopiz.kprogresshud.KProgressHUD;

public class PermissionActivity extends AppCompatActivity {
    KProgressHUD hud;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Waiting for your Permission..");
        hud.show();

        PermissionThread permissionTask = new PermissionThread(getApplicationContext(), PermissionActivity.this, hud);
        Thread backgroundThread = new Thread(permissionTask);
        backgroundThread.start();
    }

}
