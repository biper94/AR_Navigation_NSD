package com.example.jun.githubtest2;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.skp.Tmap.TMapGpsManager;
import com.skp.Tmap.TMapMarkerItem;
import com.skp.Tmap.TMapPoint;
import com.skp.Tmap.TMapView;

import java.util.List;

public class MapActivity extends AppCompatActivity implements TMapGpsManager.onLocationChangedCallback{
    RouteList route;
    List<Point> routeList;
    String Key = "7ac8c26c-7c1a-3f03-9f90-e6696b0d4329";

    private RelativeLayout map_view = null;
    private Context mContext = null;
    private boolean m_bTrackingNode = true;
    private TMapGpsManager tmapgps = null;
    private TMapView tmapview = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = getIntent();
        route = (RouteList)intent.getSerializableExtra("RouteList");
        routeList = route.getList();

        mContext = this;

        map_view = (RelativeLayout) findViewById(R.id.map_view);
        //tmapview = new TMapView(this);
        tmapview = new TMapView(getApplicationContext());
        map_view.addView(tmapview);
        tmapview.setSKPMapApiKey(Key);

        double startY = 129.079818;
        double startX = 35.234078;
        double endX = 35.230079;
        double endY = 129.08439;

        tmapview.setCompassMode(true);
        tmapview.setIconVisibility(true);
        tmapview.setZoomLevel(16);
        tmapview.setMapType(TMapView.MAPTYPE_STANDARD);
        tmapview.setLanguage(TMapView.LANGUAGE_KOREAN);
        tmapview.setTrackingMode(true);
        tmapview.setSightVisible(true);

        tmapgps = new TMapGpsManager(MapActivity.this);
        tmapgps.setMinTime(1000);
        tmapgps.setMinDistance(5);
        tmapgps.setProvider(tmapgps.NETWORK_PROVIDER); // 연결된 인터넷으로 현위치 받음(실내에서 유용)
        //tmapgps.setProvider(tmapgps.GPS_PROVIDER);

        tmapgps.OpenGps(); // 위치탐색 시작
        int MidStopSize = routeList.size();
        TMapMarkerItem[] markers = new TMapMarkerItem[MidStopSize];
        TMapPoint[] points = new TMapPoint[MidStopSize];



        for (int i = 0; i < MidStopSize; i++){
            double lat = routeList.get(i).latitude;
            double lng = routeList.get(i).longitude;
            markers[i] = new TMapMarkerItem();
            points[i] = new TMapPoint(lat, lng);
            markers[i].setTMapPoint(points[i]);
            markers[i].setVisible(TMapMarkerItem.VISIBLE);
            tmapview.addMarkerItem("tourMarker" + i, markers[i]);

        }
    }

    @Override
    public void onLocationChange(Location location) {
        if(m_bTrackingNode){
            tmapview.setLocationPoint(location.getLongitude(), location.getLatitude());

        }
    }
}
