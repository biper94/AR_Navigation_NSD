package com.example.jun.githubtest2;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

/**
 * Created by Jun on 2017-06-25.
 */

public class ApiThread implements Runnable {
    private Point curLoc;
    private Point destLoc;
    HttpURLConnection conn;
    LinkedList<Point> list;
    Context context;

    public ApiThread(Point curLoc, Point destLoc, HttpURLConnection conn, LinkedList<Point> list, Context context) {
        this.curLoc = curLoc;
        this.destLoc = destLoc;
        this.conn = conn;
        this.list = list;
        this.context = context;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void run() {
        InputStream is = null;
        String result = "";
        String apiKey = "7ac8c26c-7c1a-3f03-9f90-e6696b0d4329";
        JSONArray responseJSON;
        String urls = "https://apis.skplanetx.com/tmap/routes/pedestrian" +
                "?version=1" +
                "&startX=" +
                curLoc.longitude +
                "&startY=" +
                curLoc.latitude +
                "&endX=" +
                destLoc.longitude +
                "&endY=" +
                destLoc.latitude +
                "&startName=%EC%B6%9C%EB%B0%9C&endName=%EB%8F%84%EC%B0%A9" +
                "&appKey=7ac8c26c-7c1a-3f03-9f90-e6696b0d4329" +
                "&reqCoordType=WGS84GEO&resCoordType=WGS84GEO";

        try {
            URL url = new URL(urls);
            conn = (HttpURLConnection)url.openConnection();

            String json = "";


            conn.setConnectTimeout(60000);
            conn.setReadTimeout(20000);
            conn.setRequestMethod("POST");

            //서버 Response Data를 JSON 형식의 타입으로 요청
            conn.setRequestProperty("Accept", "application/json");

            //타입설정 application/json 형식으로 전송 (Request Body 전달 시 application/json로 서버 전달
            conn.setRequestProperty("Content-type", "application/json");

            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("utf-8"));
            os.flush();
            Log.d("conn = ", conn.toString());
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                Log.d("responseCode", "OK");
                is = conn.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] byteBuffer = new byte[1024];
                byte[] byteData = null;
                int nLength = 0;
                while ((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                    baos.write(byteBuffer, 0, nLength);
                }
                byteData = baos.toByteArray();

                result = new String(byteData);
                Log.d("result: ", result);
                JSONObject root = new JSONObject(result);
                JSONArray rootFeatures = root.getJSONArray("features");
                //Log.e("JSON: ", rootFeatures.toString());
                for (int i = 0; i < rootFeatures.length(); i++) {
                    JSONObject objElements = rootFeatures.getJSONObject(i);
                    JSONObject geometry = objElements.getJSONObject("geometry");
                    String type = geometry.getString("type");
                    if (type.equals("LineString")) {
                        JSONArray coord = geometry.getJSONArray("coordinates");
                        Log.e("Linestring", coord.toString());
                        JSONArray dest = coord.getJSONArray(coord.length() - 1);
                        double lat = dest.getDouble(1);
                        double lon = dest.getDouble(0);
                        JSONObject properties = objElements.getJSONObject("properties");
                        String description = properties.getString("description");
                        list.add(new Point(lat, lon, description));
                    }
                }

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
