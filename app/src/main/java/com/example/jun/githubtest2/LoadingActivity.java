package com.example.jun.githubtest2;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.victor.loading.rotate.RotateLoading;
public class LoadingActivity extends AppCompatActivity {
    private RotateLoading rotateLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        rotateLoading = (RotateLoading)findViewById(R.id.rotateloading);
        rotateLoading.start();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LoadingActivity.this, PermissionActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }
}
