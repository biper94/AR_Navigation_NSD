package com.example.jun.githubtest2;

/**
 * Created by Jun on 2017-06-25.
 */

import android.util.Log;

/**
 * Created by sbk on 2017-06-25.
 */

public class MemberData {
    String address;
    String lat;
    String lng;
    public MemberData(String add, String _lat, String _lng){
        address = add;
        lat = _lat;
        lat = _lng;

    }

    public void setAddress(String addr){
        address = addr;
    }
    public void setLat(String _lat){
        lat = _lat;
    }
    public void setLng(String _lng){
        lng = _lng;
    }
    public String getAddress(){
        return address;
    }
    public String getLat(){
        return lat;
    }
    public String getLng(){
        return lng;
    }
}