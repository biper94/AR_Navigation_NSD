package com.example.jun.githubtest2;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Compass extends Activity implements LocationListener, SensorEventListener {

    private static final String TAG = "Compass";
    private static boolean DEBUG = true;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    //private Sensor mAccSensor;
    //private Sensor mMagSensor;
    private DrawSurfaceView mDrawView;
    LocationManager locMgr;
    private RouteList route;
    private List<Point> routeList;
    private Point currDest;
    private int routeNum;
    private int routeCnt = 0;
    private TextView midSpot;
    private ImageButton mapBtn;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_compass);

        midSpot = (TextView)findViewById(R.id.midspot);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        mDrawView = (DrawSurfaceView) findViewById(R.id.drawSurfaceView);



        locMgr = (LocationManager) this.getSystemService(LOCATION_SERVICE); // <2>
        LocationProvider high = locMgr.getProvider(locMgr.getBestProvider(
                LocationUtils.createFineCriteria(), true));
        requestLocation();

        ImageButton locationBtn = (ImageButton)findViewById(R.id.locBtn);
        locationBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                requestLocation();
                Toast.makeText(getApplicationContext(), "Requesting Current location..", Toast.LENGTH_LONG).show();
            }
        });
        ImageButton mapBtn = (ImageButton)findViewById(R.id.mapBtn);
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Compass.this, MapActivity.class);
                intent.putExtra("RouteList", route);
                startActivity(intent);
            }
        });
        Intent intent = getIntent();
        route = (RouteList)intent.getSerializableExtra("Route");

        routeList = route.getList();
        routeNum = routeList.size();
        routeCnt = 0;
        try {
            currDest = routeList.get(routeCnt);
        } catch(java.lang.IndexOutOfBoundsException e) {
            Intent noRoute = new Intent(Compass.this, FailActivity.class);
            startActivity(noRoute);
            finish();
            return;
        }

        mDrawView.SetDest(currDest);
        midSpot.setText(currDest.description);
        midSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Compass.this, "" + currDest.latitude + ", " +
                        currDest.longitude, Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void requestLocation() {
        try {
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, this);
            locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }

    }



    @Override
    protected void onResume() {
        if (DEBUG)
            Log.d(TAG, "onResume");
        super.onResume();

        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_UI);
        try {
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, this);
            locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }
        //mSensorManager.registerListener(this, mAccSensor, SensorManager.SENSOR_DELAY_UI);
        //mSensorManager.registerListener(this, mMagSensor, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onStop() {
        if (DEBUG)
            Log.d(TAG, "onStop");
        mSensorManager.unregisterListener(this);
        try {
            locMgr.removeUpdates(this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (DEBUG)
            Log.d(TAG, "onStop");
        mSensorManager.unregisterListener(this);
        try {
            locMgr.removeUpdates(this);
        } catch(SecurityException e) {
            Log.e("Security", "SecurityException");
        }

    }

    public void onSensorChanged(SensorEvent event) {

        if (DEBUG)
            //Log.d(TAG, "sensorChanged (" + event.values[0] + ", " + event.values[1] + ", " + event.values[2] + ")");
        if (mDrawView != null) {
            //Log.i("Orientation", ": " + event.values[0] + " " + event.values[1] + " "+ event.values[2]);
            mDrawView.setOffset(event.values[0]);
            mDrawView.invalidate();
        }
    }
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Toast.makeText(this, "Accuracy Changed!!", Toast.LENGTH_SHORT).show();
        Log.e(TAG, "Accuracy Changed!! Accuracy: " + accuracy);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "Location Changed" + location.getLatitude() + ", " + location.getLongitude());
        mDrawView.setMyLocation(location.getLatitude(), location.getLongitude());
        Log.e("Location", "" + dist(new Point(location.getLatitude(), location.getLongitude(), null), currDest));
        synchronized (this) {
            if (dist(new Point(location.getLatitude(), location.getLongitude(), null), currDest) < 50) {
                if (routeCnt == routeNum - 1) {
                    Intent intent = new Intent(Compass.this, CompleteActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    currDest = routeList.get(routeCnt);
                    mDrawView.SetDest(currDest);
                    Toast.makeText(getApplicationContext(),
                            "Arrived At one of several mid stops. Go to next mid stops",
                            Toast.LENGTH_SHORT).show();
                    midSpot.setText(currDest.description);
                }
                routeCnt++;

            }
        }
        mDrawView.invalidate();
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getApplication(), "Provider Enabled..!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getApplication(), "Provider Disabled..", Toast.LENGTH_SHORT).show();
    }
    public static double dist(Point p1, Point p2) {
        return dist(p1.latitude, p1.longitude, p2.latitude, p2.longitude);
    }
    public static double dist(double lat1, double lon1, double lat2, double lon2) {

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344 * 1000;
        return dist;
    }
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

}
