package com.example.jun.githubtest2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SearchDestActivity2 extends AppCompatActivity {
    TextView searchText;
    TextView search;
    ListView list;
    ArrayList<MemberData> datas = new ArrayList<MemberData>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_dest2);

        search = (TextView)findViewById(R.id.searchText);
        Intent intent = getIntent();
        search.setText(intent.getExtras().getString("search"));
        int size = intent.getExtras().getInt("size");
        final String addr[] = intent.getExtras().getStringArray("addresses");
        final String lats[] = intent.getExtras().getStringArray("lats");
        final String lngs[] = intent.getExtras().getStringArray("lngs");
        list = (ListView)findViewById(R.id.search_list);
        if(size > 0){
            for(int i = 0; i < size; i++){
                datas.add(new MemberData(addr[i], lats[i], lngs[i]));

            }
            final LocationAdapter adapter = new LocationAdapter(getLayoutInflater(), datas);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String lat = lats[position];
                    String lng = lngs[position];

                    Intent outIntent = getIntent();
                    outIntent.putExtra("DestLat", lat);
                    outIntent.putExtra("DestLng", lng);
                    outIntent.putExtra("Dest", addr[position]);
                    setResult(RESULT_OK, outIntent);
                    Log.e("Search2", lat + ", " + lng);
                    Toast.makeText(getApplicationContext(), "Dest 위도 : "+ lat + " 경도 : "+lng, Toast.LENGTH_SHORT).show();
                    finish();

                }
            });
        }

    }


}
