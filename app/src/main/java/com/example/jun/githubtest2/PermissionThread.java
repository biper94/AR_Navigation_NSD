package com.example.jun.githubtest2;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jun on 2017-06-25.
 */

public class PermissionThread implements Runnable{
    private Context context;
    private Activity activity;
    KProgressHUD hud;

    public PermissionThread(Context context, Activity activity, KProgressHUD hud) {
        this.context = context;
        this.activity = activity;
        this.hud = hud;
    }
    @Override
    public void run() {
        requestAllPermissions();
        hud.dismiss();
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
    private boolean checkLOCATIONPermission() {
        return ContextCompat.checkSelfPermission
                (context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }
    private boolean checkCAMERAPermission() {
        if (ContextCompat.checkSelfPermission
                (context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            return true;
        else return false;
    } private boolean checkINTERNETPermission() {
        if (ContextCompat.checkSelfPermission
                (context, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)
            return true;
        else return false;
    }

    private void requestPermissions(List<String> permissionList) {
        String[] pListArr = permissionList.toArray(new String[0]);

        ActivityCompat.requestPermissions(activity, pListArr, 0);
    }

    private void requestAllPermissions() {
        List<String> listToRequest = new ArrayList<>();

        if (checkCAMERAPermission())
            listToRequest.add(Manifest.permission.CAMERA);
        else
            Log.e("PerMission", "CAMERA GRANTED");
        if (checkLOCATIONPermission())
            listToRequest.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        else
            Log.e("PerMission", "ACCESS_COARSE_LOCATION GRANTED");
        if (checkINTERNETPermission())
            listToRequest.add(Manifest.permission.INTERNET);
        else
            Log.e("PerMission", "INTERNET GRANTED");


        if (listToRequest.size() != 0) {
            requestPermissions(listToRequest);
            while(checkCAMERAPermission() || checkLOCATIONPermission() || checkINTERNETPermission()) {
                Log.e("PerMission", "Wait for permissions..");
            }
        }

    }
}
